#!/usr/bin/env groovy

def call(script) {
  // Install awscli which is required by the other pipelines.
  script.sh """apt update && \
    apt install -y python3-pip && \
    pip3 install awscli
  """
}
